//
// Created by tibo on 28/05/18.
//

#include <boost/filesystem.hpp>

#include <ros/ros.h>
#include <kdl_conversions/kdl_msg.h>
#include <geometry_msgs/PoseStamped.h>

#include <opencv2/core.hpp>

#include <dvrk_common/ros/params.hpp>
#include "dvrk_arm_to_world_calibration/tf_conversions.hpp"

ros::Subscriber pose_subscriber;
ros::Publisher pose_publisher;

KDL::Frame transform;

void on_new_pose(const geometry_msgs::PoseStampedConstPtr& pose_in) {
    KDL::Frame pose_kdl;
    tf::poseMsgToKDL(pose_in->pose, pose_kdl);
    
    geometry_msgs::PoseStamped pose_out;
    pose_out.header = pose_in->header;
    tf::poseKDLToMsg(transform.Inverse() * pose_kdl, pose_out.pose);
    
    pose_publisher.publish(pose_out);
}

void parse_tf_file(std::string file, std::string& arm_name, KDL::Frame& tf) {
    boost::filesystem::path filepath{file};
    if (filepath.is_relative()) {
        const auto cwd = boost::filesystem::current_path();
        filepath = cwd / filepath;
    }
    
    cv::FileStorage tf_file(filepath.string(), cv::FileStorage::READ);
    if (!tf_file.isOpened()) {
        ROS_ERROR("Unable to open transformation file: %s", filepath.string().c_str());
        exit(-1);
    }
    
    if (tf_file["arm_name"].isNone() or !tf_file["arm_name"].isString()) {
        ROS_ERROR(
            "Transformation file is missing field \"arm_name\" or field has wrong type (should be string)");
        exit(-1);
    }
    tf_file["arm_name"] >> arm_name;
    
    cv::Mat tf_cv;
    if (tf_file["tf"].isNone()) {
        ROS_ERROR(
            "Transformation file is missing field \"tf\" or field has wrong type (should be 4x4 matrix)");
        exit(-1);
    }
    tf_file["tf"] >> tf_cv;
    transform = tf_to<KDL::Frame>(cv::Matx44d(tf_cv));
}

int main(int argc, char** argv) {
    ros::init(argc, argv, "apply_tf");
    
    ros::NodeHandle pnh("~");
    
    std::string arm_name;
    std::string file_path = dvrk::getRequiredParam<std::string>(pnh, "tf_file");
    parse_tf_file(file_path, arm_name, transform);
    
    ros::NodeHandle pose_in("in");
    pose_subscriber = pose_in.subscribe<geometry_msgs::PoseStamped>("pose", 1, on_new_pose);
    
    if (arm_name != pose_subscriber.getTopic()) {
        ROS_WARN(
            "Applying transformation file for arm \"%s\" to arm \"%s\".",
            arm_name.c_str(), pose_subscriber.getTopic().c_str()
        );
    }
    
    ros::NodeHandle pose_out("out");
    pose_publisher = pose_out.advertise<geometry_msgs::PoseStamped>("pose", 1, true);
    
    ros::Rate loop_rate(1000);
    while (ros::ok()) {
        loop_rate.sleep();
        ros::spinOnce();
    }
    
    return 0;
}