//
// Created by tibo on 28/05/18.
//

#include <boost/filesystem.hpp>

#include <ros/ros.h>
#include <cv_bridge/cv_bridge.h>
#include <kdl_conversions/kdl_msg.h>
#include <geometry_msgs/PoseStamped.h>
#include <sensor_msgs/image_encodings.h>

#include <opencv2/core.hpp>

#include <dvrk_common/opencv/highgui.hpp>
#include <dvrk_common/ros/params.hpp>
#include <dvrk_common/ros/camera/simple_camera.hpp>

#include "dvrk_arm_to_world_calibration/tf_conversions.hpp"

double axis_length = .025; // [m]
KDL::Frame transform;
boost::optional<KDL::Frame> current_pose;

std::string window_name = "Arm Pose Viewer";

void on_new_image(const sensor_msgs::ImageConstPtr& image, const image_geometry::PinholeCameraModel model) {
    cv::Mat canvas = cv_bridge::toCvCopy(image, sensor_msgs::image_encodings::BGR8)->image;
    
    if (current_pose) {
        // Draw the 3 axes in camera space
        const auto& arm_pose = transform.Inverse() * (*current_pose);
    
        KDL::Vector x_axis = arm_pose.p + axis_length * arm_pose.M.UnitX();
        KDL::Vector y_axis = arm_pose.p + axis_length * arm_pose.M.UnitY();
        KDL::Vector z_axis = arm_pose.p + axis_length * arm_pose.M.UnitZ();
    
        // Now project the camera space coordinates into image space
        std::vector<cv::Point3d> axis_points_cv{{
            { arm_pose.p.x(), arm_pose.p.y(), arm_pose.p.z() },
            { x_axis.x()   , x_axis.y()   , x_axis.z()    },
            { y_axis.x()   , y_axis.y()   , y_axis.z()    },
            { z_axis.x()   , z_axis.y()   , z_axis.z()    },
        }};
    
        std::vector<cv::Point2d> image_points;
        for (const auto& axis_point: axis_points_cv) {
            image_points.push_back(
                model.unrectifyPoint( model.project3dToPixel(axis_point) )
            );
        }
    
        // Finally draw the position of the tool tip
        cv::line(canvas, image_points[0], image_points[1], cv::Scalar(0, 0, 255), 3, cv::LINE_AA);
        cv::line(canvas, image_points[0], image_points[2], cv::Scalar(0, 255, 0), 3, cv::LINE_AA);
        cv::line(canvas, image_points[0], image_points[3], cv::Scalar(255, 0, 0), 3, cv::LINE_AA);
    }
    
    cv::imshow(window_name, canvas);
}

void on_new_pose(const geometry_msgs::PoseStampedConstPtr& pose_in) {
    if (!current_pose)
        current_pose = KDL::Frame();
    
    tf::poseMsgToKDL(pose_in->pose, *current_pose);
}

void parse_tf_file(std::string file, std::string& camera_name, KDL::Frame& tf) {
    boost::filesystem::path filepath{file};
    if (filepath.is_relative()) {
        const auto cwd = boost::filesystem::current_path();
        filepath = cwd / filepath;
    }
    
    cv::FileStorage tf_file(filepath.string(), cv::FileStorage::READ);
    if (!tf_file.isOpened()) {
        ROS_ERROR("Unable to open transformation file: %s", filepath.string().c_str());
        exit(-1);
    }
    
    if (tf_file["camera_name"].isNone() or !tf_file["camera_name"].isString()) {
        ROS_ERROR(
            "Transformation file is missing field \"camera_name\" or field has wrong type (should be string)");
        exit(-1);
    }
    tf_file["camera_name"] >> camera_name;
    
    cv::Matx44d tf_cv;
    if (tf_file["tf"].isNone()) {
        ROS_ERROR(
            "Transformation file is missing field \"tf\" or field has wrong type (should be 4x4 matrix)");
        exit(-1);
    }
    
    cv::Mat tf_mat;
    tf_file["tf"] >> tf_mat;
    transform = tf_to<KDL::Frame>(cv::Matx44d(tf_mat));
    
    tf = tf_to<KDL::Frame>(tf_cv);
}

int main(int argc, char** argv) {
    ros::init(argc, argv, "apply_tf");
    
    ros::NodeHandle nh;
    ros::NodeHandle pnh("~");
    
    pnh.getParam("axis_length", axis_length);
    
    std::string camera_name;
    std::string file_path = dvrk::getRequiredParam<std::string>(pnh, "tf_file");
    parse_tf_file(file_path, camera_name, transform);
    
    dvrk::SimpleCamera camera(nh, "image_raw", on_new_image);
    ros::Subscriber pose_subscriber = nh.subscribe<geometry_msgs::PoseStamped>("pose", 1, on_new_pose);
    
    if (camera_name != camera.get_image_topic()) {
        ROS_WARN(
            "Applying transformation file for camera \"%s\" to camera \"%s\".",
            camera_name.c_str(), camera.get_image_topic().c_str()
        );
    }
    
    cv::namedWindow(window_name, cv::WINDOW_NORMAL);
    
    ros::Rate loop_rate(1000);
    while (ros::ok()) {
        loop_rate.sleep();
        ros::spinOnce();
        
        switch(dvrk::waitKey(1)) {
            case dvrk::KeyCode::F:
                dvrk::toggleFullScreen(window_name);
                break;
                
            case dvrk::KeyCode::Q:
            case dvrk::KeyCode::Escape:
              ros::requestShutdown();
              break;
              
            default: break;
        }
    }
    
    return 0;
}