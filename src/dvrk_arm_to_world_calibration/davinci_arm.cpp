//
// Created by tibo on 19/04/18.
//

#include <ros/ros.h>
#include <kdl_conversions/kdl_msg.h>
#include <dvrk_common/ros/util.hpp>

#include "dvrk_arm_to_world_calibration/davinci_arm.hpp"

namespace dvrk {
    
    /*
     *                  Constructors
     */
    
    DaVinciArm::DaVinciArm(
            ros::NodeHandle nh,
            const std::string arm_name,
            const std::string dvrk_namespace
        )
        : _nh(nh), _arm_name(arm_name), _dvrk_namespace(dvrk_namespace)
    {
        std::string topic_base = ros::names::append(dvrk_namespace, arm_name);
        
        const auto current_state_topic = ros::names::append(topic_base, "/current_state");
        _current_state_sub = _nh.subscribe<std_msgs::String>(
            current_state_topic, 1, &DaVinciArm::on_current_state_updated, this);
        check_published(_current_state_sub);
    
        const auto desired_state_topic = ros::names::append(topic_base, "/desired_state");
        _desired_state_sub = _nh.subscribe<std_msgs::String>(
            desired_state_topic, 1, &DaVinciArm::on_desired_state_updated, this);
        check_published(_desired_state_sub);
        
        const auto goal_reached_topic = ros::names::append(topic_base, "/goal_reached");
        _goal_reached_sub = _nh.subscribe<std_msgs::Bool>(
            goal_reached_topic, 1, &DaVinciArm::on_goal_reached_updated, this);
        check_published(_goal_reached_sub );
        
        const auto state_joint_current_topic = ros::names::append(topic_base, "state_joint_current");
        _state_joint_current_sub = _nh.subscribe<sensor_msgs::JointState>(
            state_joint_current_topic, 1, &DaVinciArm::on_state_joint_current_updated, this);
        check_published(_state_joint_current_sub);
    
        const auto state_joint_desired_topic = ros::names::append(topic_base, "state_joint_desired");
        _state_joint_desired_sub = _nh.subscribe<sensor_msgs::JointState>(
            state_joint_desired_topic, 1, &DaVinciArm::on_state_joint_desired_updated, this);
        check_published(_state_joint_desired_sub);
    
        const auto position_cartesian_current_topic =
            ros::names::append(topic_base, "position_cartesian_current");
        _position_cartesian_current_sub = _nh.subscribe<geometry_msgs::PoseStamped>(
            position_cartesian_current_topic, 1, &DaVinciArm::on_position_cartesian_current_updated, this);
        check_published(_position_cartesian_current_sub);
    
        const auto position_cartesian_desired_topic =
            ros::names::append(topic_base, "position_cartesian_desired");
        _position_cartesian_desired_sub = _nh.subscribe<geometry_msgs::PoseStamped>(
            position_cartesian_desired_topic, 1, &DaVinciArm::on_position_cartesian_desired_updated, this);
        check_published(_position_cartesian_desired_sub);
    
        const auto position_cartesian_local_current_topic =
            ros::names::append(topic_base, "position_cartesian_local_current");
        _position_cartesian_local_current_sub = _nh.subscribe<geometry_msgs::PoseStamped>(
            position_cartesian_local_current_topic, 1, &DaVinciArm::on_position_cartesian_local_current_updated, this);
        check_published(_position_cartesian_local_current_sub);
    
        const auto position_cartesian_local_desired_topic =
            ros::names::append(topic_base, "position_cartesian_local_desired");
        _position_cartesian_local_desired_sub = _nh.subscribe<geometry_msgs::PoseStamped>(
            position_cartesian_local_desired_topic, 1, &DaVinciArm::on_position_cartesian_local_desired_updated, this);
        check_published(_position_cartesian_local_desired_sub);
    
        const auto twist_body_current_topic = ros::names::append(topic_base, "twist_body_current");
        _twist_body_current_sub = _nh.subscribe<geometry_msgs::TwistStamped>(
            twist_body_current_topic, 1, &DaVinciArm::on_twist_body_current_updated, this);
        check_published(_twist_body_current_sub);
    
        const auto wrench_body_current_topic = ros::names::append(topic_base, "wrench_body_current");
        _wrench_body_current_sub = _nh.subscribe<geometry_msgs::WrenchStamped>(
            wrench_body_current_topic, 1, &DaVinciArm::on_wrench_body_current_updated, this);
        check_published(_wrench_body_current_sub);
        
        const auto jacobian_spatial_topic = ros::names::append(topic_base, "jacobian_spatial");
        _jacobian_spatial_sub = _nh.subscribe<std_msgs::Float64MultiArray>(
            jacobian_spatial_topic, 1, &DaVinciArm::on_jacobian_spatial_updated, this);
        check_published(_jacobian_spatial_sub);
        
        const auto jacobian_body_topic = ros::names::append(topic_base, "jacobian_body");
        _jacobian_body_sub = _nh.subscribe<std_msgs::Float64MultiArray>(
            jacobian_body_topic, 1, &DaVinciArm::on_jacobian_body_updated, this);
        check_published(_jacobian_body_sub);
    }
    
    
    DaVinciArm::DaVinciArm(const std::string arm_name, const std::string dvrk_namespace)
        : DaVinciArm(ros::NodeHandle(), arm_name, dvrk_namespace)
    {
        // Do Nothing
    }
    
    bool DaVinciArm::has_messages() const {
        // We only check the topics that should be published continuously, the others are only
        // published in response to specific events and may not arrive for quite some time.
        return  (_current_joint_state_seq != 0)
            and (_position_cartesian_current_seq != 0)
            and (_position_cartesian_local_current_seq != 0);
    }
    
    /*
     *                  Public Interface
     */
    
    boost::optional<std::string> DaVinciArm::get_current_state() const {
        if (_current_joint_state_seq == 0)
            return boost::none;
        
        return { _current_state };
    }
    
    
    boost::optional<std::string> DaVinciArm::get_desired_state() const {
        if (_desired_joint_state_seq == 0)
            return boost::none;
    
        return { _desired_state };
    }
    
    
    boost::optional<std::string> DaVinciArm::get_joint_name(const size_t joint_number) const {
        if (_current_joint_state_seq == 0)
            return boost::none;
    
        return _joint_names.at(joint_number);
    }
    
    
    boost::optional<double> DaVinciArm::get_current_joint_position(const size_t joint_number) const {
        if (_current_joint_state_seq == 0)
            return boost::none;
        
        return _current_joint_position.at(joint_number);
    }
    
    
    boost::optional<double> DaVinciArm::get_current_joint_velocity(const size_t joint_number) const {
        if (_current_joint_state_seq == 0)
            return boost::none;
        
        return _current_joint_velocity.at(joint_number);
    }
    
    
    boost::optional<double> DaVinciArm::get_current_joint_effort(const size_t joint_number) const {
        if (_current_joint_state_seq == 0)
            return boost::none;
        
        return _current_joint_effort.at(joint_number);
    }
    
    
    boost::optional<double> DaVinciArm::get_desired_joint_position(const size_t joint_number) const {
        if (_desired_joint_state_seq == 0)
            return boost::none;
        
        return _desired_joint_position.at(joint_number);
    }
    
    
    boost::optional<double> DaVinciArm::get_desired_joint_velocity(const size_t joint_number) const {
        if (_desired_joint_state_seq == 0)
            return boost::none;
    
        return _desired_joint_velocity.at(joint_number);
    }
    
    
    boost::optional<double> DaVinciArm::get_desired_joint_effort(const size_t joint_number) const {
        if (_desired_joint_state_seq == 0)
            return boost::none;
    
        return _desired_joint_effort.at(joint_number);
    }
    
    
    boost::optional<KDL::Frame> DaVinciArm::get_current_position() const {
        if (_position_cartesian_current_seq > 0)
            return { _position_cartesian_current };
        
        return boost::none;
    }
    
    
    boost::optional<KDL::Frame> DaVinciArm::get_current_local_position() const {
        if (_position_cartesian_local_current_seq > 0)
            return { _position_cartesian_local_current };
    
        return boost::none;
    }
    
    
    boost::optional<KDL::Frame> DaVinciArm::get_desired_position() const {
        if (_position_cartesian_desired_seq > 0)
            return { _position_cartesian_desired };
        
        return boost::none;
    }
    
    
    boost::optional<KDL::Frame> DaVinciArm::get_desired_local_position() const {
        if (_position_cartesian_local_desired_seq > 0)
            return { _position_cartesian_local_desired };
        
        return boost::none;
    }
    
    /*
     *                  Private Methods - ROS Callbacks
     */

    void DaVinciArm::on_current_state_updated(const std_msgs::String state) {
        _current_state = state.data;
        _current_state_seq += 1;
    }
    
    
    void DaVinciArm::on_desired_state_updated(const std_msgs::String state) {
        _desired_state = state.data;
        _desired_state_seq += 1;
    }
    
    
    void DaVinciArm::on_goal_reached_updated(const std_msgs::Bool reached) {
        _goal_reached = reached.data;
    }
    
    
    void DaVinciArm::on_state_joint_current_updated(const sensor_msgs::JointState joint_state) {
        if (_joint_names.empty())
            _joint_names = joint_state.name;
        
        if (_joint_names.size() != joint_state.name.size()) {
            ROS_ERROR("Joint names vector has changed - mappings from joint names to joint numbers may be off");
        }
        
        _current_joint_state_seq = joint_state.header.seq;
        _current_joint_position = joint_state.position;
        _current_joint_velocity = joint_state.velocity;
        _current_joint_effort   = joint_state.effort;
    }
    
    
    void DaVinciArm::on_state_joint_desired_updated(const sensor_msgs::JointState joint_state) {
        if (_joint_names.empty())
            _joint_names = joint_state.name;
    
        if (_joint_names.size() != joint_state.name.size()) {
            ROS_ERROR("Joint names vector has changed - mappings from joint names to joint numbers may be off");
        }
    
        _desired_joint_state_seq = joint_state.header.seq;
        _desired_joint_position = joint_state.position;
        _desired_joint_velocity = joint_state.velocity;
        _desired_joint_effort   = joint_state.effort;
    }
    
    
    void DaVinciArm::on_position_cartesian_desired_updated(const geometry_msgs::PoseStamped pose) {
        tf::poseMsgToKDL(pose.pose, _position_cartesian_desired);
        _position_cartesian_desired_seq = pose.header.seq;
    }
    
    
    void DaVinciArm::on_position_cartesian_current_updated(const geometry_msgs::PoseStamped pose) {
        tf::poseMsgToKDL(pose.pose, _position_cartesian_current);
        _position_cartesian_current_seq = pose.header.seq;
    }
    
    
    void DaVinciArm::on_position_cartesian_local_desired_updated(const geometry_msgs::PoseStamped pose) {
        tf::poseMsgToKDL(pose.pose, _position_cartesian_desired);
        _position_cartesian_local_desired_seq = pose.header.seq;
    }
    
    
    void DaVinciArm::on_position_cartesian_local_current_updated(const geometry_msgs::PoseStamped pose) {
        tf::poseMsgToKDL(pose.pose, _position_cartesian_local_current);
        _position_cartesian_local_current_seq = pose.header.seq;
    }
    
    
    void DaVinciArm::on_twist_body_current_updated(const geometry_msgs::TwistStamped twist) {
        tf::twistMsgToKDL(twist.twist, _twist_body);
    }
    
    
    void DaVinciArm::on_wrench_body_current_updated(const geometry_msgs::WrenchStamped wrench) {
        tf::wrenchMsgToKDL(wrench.wrench, _wrench_body);
    }
    
    
    void DaVinciArm::on_jacobian_spatial_updated(const std_msgs::Float64MultiArray jacobian) {
        // TODO: Use KDL::Jacobian
    }
    
    
    void DaVinciArm::on_jacobian_body_updated(const std_msgs::Float64MultiArray jacobian) {
        // TODO: Use KDL::Jacobian
    }
}
