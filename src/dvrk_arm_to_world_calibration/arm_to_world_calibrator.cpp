//
// Created by nima on 16/11/17.
//

#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>

#include <dvrk_common/opencv/highgui.hpp>
#include <dvrk_common/opencv/util.hpp>
#include <dvrk_camera_calibration/calibration_image.hpp>

#include "dvrk_arm_to_world_calibration/tf_conversions.hpp"
#include "dvrk_arm_to_world_calibration/arm_to_world_calibrator.hpp"

namespace dvrk {
    
    
    ArmToWorldCalibrator::ArmToWorldCalibrator(
        std::shared_ptr<DaVinciArm>& arm,
        std::shared_ptr<SimpleCamera>& camera,
        const CalibrationPattern pattern
    )
        : _camera(camera), _arm(arm), _pattern(pattern)
    {
        _camera->set_on_image_callback(&ArmToWorldCalibrator::on_new_image, this);
        
        cv::Point3f board_centre_point = {
            std::floor(_pattern.size.width / 2.0f)  * _pattern.feature_size(),
            std::floor(_pattern.size.height / 2.0f) * _pattern.feature_size(),
            0.0f
        };
        
        // to calibrate we draw two lines of 3 points centered around the centre point of the pattern
        const int points_per_line = 3;
        const int x_offset_multiplier = static_cast<int>(
            std::floor((_pattern.size.width + 1) / static_cast<float>(points_per_line)));

        const float x_offset = _pattern.feature_size() * x_offset_multiplier;
        const float y_offset = _pattern.feature_size();
        
        const auto first_line_start = board_centre_point - cv::Point3f(x_offset, y_offset, 0.0f);
        const auto second_line_start = board_centre_point - cv::Point3f(x_offset, -y_offset, 0.0f);
        
        _camera_points = {
            first_line_start + 0 * cv::Point3f(x_offset, 0.0f, 0.0f),
            first_line_start + 1 * cv::Point3f(x_offset, 0.0f, 0.0f),
            first_line_start + 2 * cv::Point3f(x_offset, 0.0f, 0.0f),
            second_line_start + 0 * cv::Point3f(x_offset, 0.0f, 0.0f),
            second_line_start + 1 * cv::Point3f(x_offset, 0.0f, 0.0f),
            second_line_start + 2 * cv::Point3f(x_offset, 0.0f, 0.0f),
        };
    }
    
    
    bool ArmToWorldCalibrator::start() {
        if (!_current_board_pose)
            return false;
            
        _progress = CalibrationProgress::InProgress;
        _initial_board_pose = *_current_board_pose;
        _image_points = _latest_image.projectPoints(_camera_points);
        
        return true;
    }
    
    
    cv::Mat ArmToWorldCalibrator::update() {
        cv::Mat image = _latest_image.get_image();
        
        switch (_progress) {
            case CalibrationProgress::WaitingForStart: {
                _latest_image.draw_corners(image, false, 10);
                
                if (_latest_image.has_pattern())
                    _latest_image.draw_axis(image, (_pattern.feature_size_mm / 1000) * 2);
                
                return image;
            }
        
            case CalibrationProgress::InProgress: {
                const auto draw_target = [&image](cv::Point2f target, cv::Scalar colour) {
                    dvrk::draw_cross(image, target, 26, colour);
                    cv::circle(image, target, 10, colour, 1, cv::LINE_AA);
                };
                
                // Draw the points we've already pointed
                for (size_t i = 0; i < _arm_points.size(); ++i)
                    draw_target(_image_points[i], dvrk::Colour::LawnGreen);
                
                // Draw the one we need to target (only if it won't cause an out-of-bounds read)
                if (_arm_points.size() < _camera_points.size())
                    draw_target(_image_points[_arm_points.size()], dvrk::Colour::Gold);
                
                // Draw the remaining points
                for (size_t i = _arm_points.size() + 1; i < _camera_points.size(); ++i)
                    draw_target(_image_points[i], dvrk::Colour::OrangeRed);
    
                return image;
            }
        
            case CalibrationProgress::Complete: {
                const auto has_pos = _arm->get_current_position();
                if (!has_pos)
                    return image;
                
                // Project the arm position into camera space
                KDL::Frame arm_pos_in_arm_space = *has_pos;
                KDL::Frame arm_pos_in_cam_space = (*_world_to_arm).Inverse() * arm_pos_in_arm_space;
                
                // Draw the 3 axes in camera space
                const auto& arm_pos = arm_pos_in_cam_space;
                
                const auto axis_length = _pattern.feature_size();
                KDL::Vector x_axis = arm_pos.p + axis_length * arm_pos.M.UnitX();
                KDL::Vector y_axis = arm_pos.p + axis_length * arm_pos.M.UnitY();
                KDL::Vector z_axis = arm_pos.p + axis_length * arm_pos.M.UnitZ();
                
                // Now project the camera space coordinates into image space
                std::vector<cv::Point3d> arm_pos_cv{{
                     { arm_pos.p.x(), arm_pos.p.y(), arm_pos.p.z() },
                     { x_axis.x()   , x_axis.y()   , x_axis.z()    },
                     { y_axis.x()   , y_axis.y()   , y_axis.z()    },
                     { z_axis.x()   , z_axis.y()   , z_axis.z()    },
                }};
    
                std::vector<cv::Point2d> image_points(4);
                cv::projectPoints(
                    /* [in] objectPoints = */ arm_pos_cv,
                    /* [in] rvec         = */ _initial_board_pose.rotation,
                    /* [in] tvec         = */ _initial_board_pose.translation,
                    /* [in] cameraMatrix = */ (*_camera_params).camera_matrix,
                    /* [in] distCoeffs   = */ (*_camera_params).distortion_coefficients,
                    /* [out] imagePoints = */ image_points
                );
                
                // Finally draw the position of the tool tip
                cv::line(image, image_points[0], image_points[1], cv::Scalar(0, 0, 255), 3, cv::LINE_AA);
                cv::line(image, image_points[0], image_points[2], cv::Scalar(0, 255, 0), 3, cv::LINE_AA);
                cv::line(image, image_points[0], image_points[3], cv::Scalar(255, 0, 0), 3, cv::LINE_AA);
                
                return image;
            }
        }
    }
    
    
    bool ArmToWorldCalibrator::pick_point() {
        if (_progress != CalibrationProgress::InProgress)
            return false;
        
        if ((ros::Time::now() - _last_pick).toSec() < 2.)
            return false;
        
        const auto success = _arm->get_current_position();
        if (!success)
            return false;
        
        KDL::Frame pos = *success;
        _arm_points.emplace_back(pos.p.x(), pos.p.y(), pos.p.z());
        
        if (_arm_points.size() == _camera_points.size())
            _world_to_arm = calibrate();
        
        return true;
    }
    
    
    boost::optional<KDL::Frame> ArmToWorldCalibrator::calibrate() {
        if( _camera_points.size()!= _arm_points.size())
            return {};
    
        Eigen::Matrix<double, 3, 6> points_in_camera_frame_mat;
        Eigen::Matrix<double, 3, 6> points_in_arm_frame_mat;
    
        for (uint i = 0; i < _camera_points.size(); i++) {
            const auto camera_pt = _camera_points[i];
            points_in_camera_frame_mat.col(i) =
                Eigen::Vector3d{ camera_pt.x, camera_pt.y, camera_pt.z };
    
            const auto arm_pt = _arm_points[i];
            points_in_arm_frame_mat.col(i) =
                Eigen::Vector3d{ arm_pt.x, arm_pt.y, arm_pt.z };
        }
        
        // Compute the transformation between the two sets of points
        Eigen::Matrix4d camera_to_arm = Eigen::umeyama(points_in_camera_frame_mat, points_in_arm_frame_mat, false);

        
        _world_to_arm = tf_to<KDL::Frame>(camera_to_arm);
        _progress = CalibrationProgress::Complete;
        return _world_to_arm;
    }
    
    
    bool ArmToWorldCalibrator::save_transformation(boost::filesystem::path filepath) {
        if (_progress != CalibrationProgress::Complete or !_world_to_arm)
            return false;
        
        if (filepath.is_relative()) {
            const auto cwd = boost::filesystem::current_path();
            filepath = cwd / filepath;
        }
    
        cv::FileStorage outfile(filepath.string(), cv::FileStorage::WRITE);
        if (!outfile.isOpened()) {
            return false;
        }
    
        outfile << "camera_name" << _camera->get_image_topic()
                << "arm" << _arm->get_current_position_topic()
                << "tf" << cv::Mat( tf_to<cv::Matx44d>(*_world_to_arm) );
        outfile.release();
        
        return true;
    }
    
    
    void ArmToWorldCalibrator::on_new_image(
        const sensor_msgs::ImageConstPtr& image,
        const image_geometry::PinholeCameraModel& model
    ) {
        const auto cv_image = cv_bridge::toCvShare(image, sensor_msgs::image_encodings::BGR8);
        _camera_params = CameraParams::FromPinholeCameraModel(model);
        _latest_image = dvrk::CalibrationImage(_pattern, cv_image->image);
        _latest_image.set_intrinsics( *_camera_params );
        
        _current_board_pose = { _latest_image.get_pose() };
    }
}
