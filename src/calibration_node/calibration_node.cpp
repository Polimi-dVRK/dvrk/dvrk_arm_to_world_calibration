//
// Created by tibo on 21/05/18.
//

#include <dvrk_common/opencv/highgui.hpp>

#include "calibration_node_ui.hpp"
#include "dvrk_arm_to_world_calibration/arm_to_world_calibrator.hpp"

const std::string window_name = "Arm To World Calibration";

int main(int argc, char** argv) {
    
    ros::init(argc, argv, "arm_to_world_calibration");
    
    ros::NodeHandle pnh("~");
    
    std::string arm_name;
    if (!pnh.getParam("arm_name", arm_name)) {
        ROS_ERROR("Required parameter \"arm_name\" missing.");
        return 1;
    }
    
    std::string transformation_filename{ "world_to_arm_transformation.yaml" };
    pnh.getParam("filename", transformation_filename);
    
    std::string pattern_spec;
    if (!pnh.getParam("pattern_spec", pattern_spec)) {
        ROS_ERROR("Required parameter \"pattern_spec\" missing.");
        return 1;
    }
    const auto pattern = dvrk::CalibrationPattern::FromString(pattern_spec);
    
    auto arm = std::make_shared<dvrk::DaVinciArm>(arm_name, "/dvrk");
    auto camera = std::make_shared<dvrk::SimpleCamera>(ros::NodeHandle(), "image_raw");
    
    dvrk::ArmToWorldCalibrator calibrator(arm, camera, pattern);
    CalibrationNodeUI ui;
    ui.set_pattern(pattern);
    
    cv::namedWindow(window_name, cv::WINDOW_NORMAL);
    
    ros::Rate loop_rate(100);
    while (ros::ok() and !camera->has_images()) {
        const auto keycode = dvrk::waitKey(1);
        if (keycode == dvrk::KeyCode::Escape or keycode == dvrk::KeyCode::Q)
            ros::shutdown();
        
        ROS_INFO_THROTTLE(5, "Waiting for images on %s ...", camera->get_image_topic().c_str());
        loop_rate.sleep();
        ros::spinOnce();
    }
    
    while (ros::ok() and !arm->has_messages()) {
        const auto keycode = dvrk::waitKey(1);
        if (keycode == dvrk::KeyCode::Escape or keycode == dvrk::KeyCode::Q)
            ros::shutdown();
        
        ROS_INFO_THROTTLE(5, "Waiting for arm messages ...");
        loop_rate.sleep();
        ros::spinOnce();
    }
    
    // Ready to start
    
    ui.add_log("Move the pattern around until it is detected then press 'B' to begin", MessageType::Info);
//    try {
        while (ros::ok()) {
            const auto keycode = dvrk::waitKey(1);
            switch (keycode) {
                case dvrk::KeyCode::B:
                    if (calibrator.start())
                        ui.add_log("Calibration started. Move the arm tip to the RED point and press <P>", MessageType::Success);
                    else
                        ui.add_log("Unable to start calibration. Is the board visible ?", MessageType::Error);
                    
                    break;
            
                case dvrk::KeyCode::F:
                    dvrk::toggleFullScreen(window_name);
                    break;
                    
                case dvrk::KeyCode::P:
                    if (calibrator.pick_point()) {
                        std::ostringstream oss;
                        oss << "Picked point " << calibrator.get_arm_points().size()
                            << " of " << calibrator.get_camera_points().size();
                        ui.add_log(oss.str(), MessageType::Success);
                    } else {
                        ui.add_log("Pick Failed", MessageType::Error);
                    }
                    
                    break;
                
                case dvrk::KeyCode::S:
                    if (calibrator.save_transformation(transformation_filename)) {
                        std::ostringstream oss;
                        oss << "Transformation information written to: " << transformation_filename;
                        ui.add_log(oss.str(), MessageType::Success);
                    } else {
                        ui.add_log("Unable to save calibration results file.", MessageType::Error);
                    }
                    break;
                
                case dvrk::KeyCode::Q:
                case dvrk::KeyCode::Escape:
                    ros::shutdown();
                    break;
            
                default: break;
            }
        
            ui.set_hero_image(calibrator.update());
            cv::imshow(window_name, ui.render());
    
            loop_rate.sleep();
            ros::spinOnce();
        }
//    } catch (const dvrk::runtime_error& err) {
//        std::cerr << boost::diagnostic_information(err) << std::endl;
//        return -1;
//    }
    
    return 0;
}
