//
// Created by tibo on 21/05/18.
//

#pragma once

#include <ros/ros.h>

#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>

#include <dvrk_camera_calibration/calibration_pattern.hpp>

enum class MessageType {
    Success, Info, Error
};

class CalibrationNodeUI {
public:
    CalibrationNodeUI() = default;
    
    CalibrationNodeUI& set_hero_image(cv::Mat hero_image) {
        assert(!hero_image.empty());
        const auto scale_factor = hero_image_height / hero_image.rows;
        cv::resize(hero_image, _hero_image, cv::Size(), scale_factor, scale_factor, cv::INTER_CUBIC);
    
        return *this;
    }
    
    CalibrationNodeUI& set_pattern(const dvrk::CalibrationPattern& pattern) {
        _pattern_type_str = std::string("Type: ") + to_string(pattern.type);
        _pattern_size_str = std::string("Size: ") +
            std::to_string(pattern.size.width) + "x" + std::to_string(pattern.size.height);
        
        std::ostringstream oss;
        oss << "Feature Size: " << std::fixed << std::setprecision(1) << pattern.feature_size_mm << "mm";
        _pattern_feature_size_str = oss.str();
        
        return *this;
    }
    
    CalibrationNodeUI& add_log(const std::string line, const MessageType type) {
        _log_lines.emplace_back(std::move(line), type);
        
        switch(type) {
            case MessageType::Success:
            case MessageType::Info:
                ROS_INFO("%s", line.c_str());
                break;
            
            case MessageType::Error:
                ROS_ERROR("%s", line.c_str());
                break;
        }
        
        
        while (_log_lines.size() > 5)
            _log_lines.pop_front();
        
        return *this;
    }
    
    
    const cv::Mat& render() {
        
        // Compute _ui size
        const auto ui_window_width =
            (margin.left + _hero_image.cols + gutter_size) + (pattern_group_width + margin.right);
        
        const auto ui_window_height =
            (margin.top + _hero_image.rows + gutter_size)
                + (padding.top + num_log_lines * line_height + padding.bottom + margin.bottom);
        
        _ui_canvas = cv::Mat(ui_window_height, ui_window_width, CV_8UC3);
        _ui_canvas.setTo(background_colour);
        
        // Draw the image onto the canvas
        const cv::Rect hero_image_target(margin.left, margin.top, _hero_image.cols, _hero_image.rows);
        _hero_image.copyTo( _ui_canvas(hero_image_target) );
        
        // Draw the pattern information to it's own box
        const cv::Point pattern_box_tl(
            margin.left + _hero_image.cols + gutter_size, margin.top);
        
        cv::Rect pattern_box_area(
            pattern_box_tl, cv::Size(pattern_group_width, 6 * line_height) );
        _ui_canvas( pattern_box_area ).setTo(block_background);
        
        cv::Point2i text_coord(pattern_box_tl.x + padding.left, pattern_box_tl.y + padding.top + 1.5f * line_height);
        put_text(text_coord, "Pattern Details", text_colour, 1.5);
        
        text_coord.y += line_height * 1.5;
        put_text(text_coord, _pattern_type_str, text_colour);
        
        text_coord.y += line_height;
        put_text(text_coord, _pattern_size_str, text_colour);
        
        text_coord.y += line_height;
        put_text(text_coord, _pattern_feature_size_str, text_colour);
        
        // Draw the usage box
        cv::Point keys_box_tl(
            pattern_box_tl.x, pattern_box_tl.y + pattern_box_area.height + margin.bottom);
        cv::Rect keys_box_area(
            keys_box_tl,
            cv::Size(pattern_group_width, padding.top + line_height * 3.f + padding.bottom)
        );
        _ui_canvas( keys_box_area ).setTo(block_background);
        
        text_coord = keys_box_tl;
        text_coord.x += padding.left;
        text_coord.y += padding.top + line_height;
        put_text(text_coord, "S - Save Image", text_colour);
        
        text_coord.y += line_height;
        put_text(text_coord, "F - Toggle Full Screen", text_colour);
        
        text_coord.y += line_height;
        put_text(text_coord, "Q - Exit", text_colour);
        
        // Draw the log box
        cv::Point log_box_tl(
            margin.left, margin.top + _hero_image.rows + gutter_size);
        cv::Rect log_box_area(
            log_box_tl,
            cv::Size(_hero_image.cols, padding.top + num_log_lines * line_height + padding.bottom)
        );
        _ui_canvas( log_box_area ).setTo(block_background);
        
        text_coord = log_box_tl;
        text_coord.x += padding.left;
        text_coord.y += padding.top + line_height;
        
        for (const auto& line: _log_lines) {
            put_text(text_coord, line.first, to_colour(line.second));
            text_coord.y += line_height;
        }
        
        return _ui_canvas;
    }

public:
    const cv::Scalar text_colour    = {   0,   0,   0 };
    const cv::Scalar success_colour = {  50, 205,  50 };
    const cv::Scalar error_colour   = {  60, 20, 220 };
    
    const cv::Scalar background_colour = { 240, 241, 242 };
    const cv::Scalar block_background  = { 225, 225, 225 };
    
    const int font_face = cv::HersheyFonts::FONT_HERSHEY_DUPLEX;
    const float base_font_size = 0.5;
    
    struct {
        const unsigned int top    = 15;
        const unsigned int right  = 15;
        const unsigned int bottom = 15;
        const unsigned int left   = 15;
    } margin;
    
    struct {
        const unsigned int top    = 5;
        const unsigned int right  = 5;
        const unsigned int bottom = 7;
        const unsigned int left   = 5;
    } padding;
    
    const unsigned int pattern_group_width = 200;
    const unsigned int num_log_lines = 5;
    
    const unsigned int line_height = 15;
    const unsigned int gutter_size = 15;
    const unsigned int hero_image_height = 1024;

private:
    
    cv::Size put_text(cv::Point2i pos, std::string text, cv::Scalar colour, float font_scale = 1.0) {
        const auto font_size = base_font_size * font_scale;
        
        const auto text_size = cv::getTextSize(text, font_face, font_scale, 1, nullptr);
        
        cv::putText(
            /* img = */ _ui_canvas,
            /* text = */ text,
            /* org = */ pos,
            /* fontFace = */ font_face,
            /* fontScale = */ font_size,
            /* color = */ colour,
            /* thickness = */ 1,
            /* lineType = */ cv::LINE_AA
        );
        
        return text_size;
    }
    
    cv::Scalar to_colour(const MessageType type) {
        switch (type) {
            case MessageType::Success: return success_colour;
            case MessageType::Error:   return error_colour;
            default:                   return text_colour;
        }
    };
    
private:
    
    cv::Mat _hero_image;
    cv::Mat _ui_canvas;
    
    std::string _pattern_type_str = "N/A";
    std::string _pattern_size_str = "N/A";
    std::string _pattern_feature_size_str = "N/A";
    
    std::vector<std::string> _usage_lines{
        "B - Begin Calibration",
        "P - Pick Point",
        "S - Save Results",
        "Q - Exit"
    };
    
    std::list< std::pair<std::string, MessageType> > _log_lines;
};


