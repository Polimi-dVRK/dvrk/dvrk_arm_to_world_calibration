//
// Created by tibo on 28/05/18.
//

#pragma once

#include <opencv2/core.hpp>
#include <kdl/frames.hpp>
#include <Eigen/Dense>

inline cv::Matx44d tf_to(cv::Matx33d rot, cv::Vec3d trasl) {
    return {
        rot(0, 0), rot(0, 1), rot(0, 2), trasl(0),
        rot(1, 0), rot(1, 1), rot(1, 2), trasl(1),
        rot(2, 0), rot(2, 1), rot(2, 2), trasl(2),
               0.,        0.,        0.,       1.,
    };
}

// Template magix time
//
// We want to have a bunch of functions that differ only in their return type. Normal overloading
// can't do that since overloads nly consider the function signature (parameter types,
// cv-qualifiers, function name). The solution is _templates_.
//
// We declare a template function parametrised on the output type and implement the specialisations
// we are interested in. All other uses will fail at compile time. The downsie is that we need to
// manually specify the return type when calling the function.

template <class DstType>
DstType tf_to(const Eigen::Matrix4d& src);

template <>
cv::Matx44d tf_to<cv::Matx44d>(const Eigen::Matrix4d& src) {
    return {
        src(0, 0), src(0, 1), src(0, 2), src(0, 3),
        src(1, 0), src(1, 1), src(1, 2), src(1, 3),
        src(2, 0), src(2, 1), src(2, 2), src(2, 3),
               0.,        0.,        0.,        1.,
    };
}

template <>
KDL::Frame tf_to<KDL::Frame>(const Eigen::Matrix4d& src) {
    KDL::Frame dst;
    dst.p = { dst(0, 3), dst(1, 3), dst(2, 3), };
    dst.M = {
        dst(0, 0), dst(0, 1), dst(0, 2),
        dst(1, 0), dst(1, 1), dst(1, 2),
        dst(2, 0), dst(2, 1), dst(2, 2),
    };
    return dst;
}

template <class DstType>
DstType tf_to(const KDL::Frame& src);

template <>
cv::Matx44d tf_to<cv::Matx44d>(const KDL::Frame& src) {
    return {
        src.M(0, 0), src.M(0, 1), src.M(0, 2), src.p.x(),
        src.M(1, 0), src.M(1, 1), src.M(1, 2), src.p.y(),
        src.M(2, 0), src.M(2, 1), src.M(2, 2), src.p.z(),
                 0.,          0.,          0.,        1.,
    };
}

template <class DstType>
DstType tf_to(const cv::Matx44d& src);

template <>
KDL::Frame tf_to<KDL::Frame>(const cv::Matx44d& src) {
    KDL::Frame dst;
    dst.p = { dst(0, 3), dst(1, 3), dst(2, 3), };
    dst.M = {
        dst(0, 0), dst(0, 1), dst(0, 2),
        dst(1, 0), dst(1, 1), dst(1, 2),
        dst(2, 0), dst(2, 1), dst(2, 2),
    };
    return dst;
}

