//
// Created by tibo on 19/04/18.
//

#pragma once

#include <memory>
#include <ros/ros.h>

#include <kdl/frames.hpp>

#include <std_msgs/Bool.h>
#include <std_msgs/String.h>
#include <std_msgs/Float64MultiArray.h>

#include <sensor_msgs/JointState.h>

#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/WrenchStamped.h>
#include <geometry_msgs/TwistStamped.h>

namespace dvrk {
    
    enum class DaVinciArmType {
        ECM, MTML, MTMR, PSM1, PSM2
    };
    
    class DaVinciArm : boost::noncopyable {
    
    public:
        
        DaVinciArm() = default;
        
        explicit DaVinciArm(const std::string arm_name, const std::string dvrk_namespace = "/dvrk");
        
        DaVinciArm(
            ros::NodeHandle nh,
            const std::string arm_name,
            const std::string dvrk_namespace = "/dvrk"
        );

        
        /*
         *              Get Topic Names
         */
        
        /// Get the resolved topic from which we are reading the current state
        std::string get_current_state_topic() const { return _current_state_sub.getTopic(); }
        
        /// Get the resolved topic from which we are reading the desired state
        std::string get_desired_state_topic() const { return _desired_state_sub.getTopic(); }
        
        /// Get the resolved topic from which we are reading the goal eached events
        std::string get_goal_reached_topic() const { return _goal_reached_sub.getTopic(); }
        
        /// Get the resolved topic from which we are reading the current joint state
        std::string get_current_joint_state_topic() const {
            return _state_joint_current_sub.getTopic();
        }
        
        /// Get the resolved topic from which we are reading the desired joint state
        std::string get_desired_joint_state_topic() const {
            return _state_joint_desired_sub.getTopic();
        }
        
        /// Get the resolved topic from which we are reading the current cartesian position
        std::string get_current_position_topic() const {
            return _position_cartesian_current_sub.getTopic();
        }
        
        /// Get the resolved topic from which we are reading the desired cartesian position
        std::string get_desired_position_topic() const {
            return _position_cartesian_desired_sub.getTopic();
        }
        
        /*
         *              Get State Information
         */
        
        /// Check whether any messages have been received so far
        bool has_messages() const;
        
        /// Get the name of the arm
        /// @see DaVinciArm::_arm_name
        std::string get_name() const { return _arm_name; }
        
        /// Get the namespace that arm has been placed into
        /// @see DaVinciArm::_dvrk_namespace
        std::string get_namespace() const {
            return ros::names::resolve( ros::names::append(_dvrk_namespace, _arm_name) );
        }
        
        boost::optional<std::string> get_current_state() const;
        
        boost::optional<std::string> get_desired_state() const;
        
        /**
         * Get the names of the of the i-th joint as reported by the joint state messages.
         * If no messages have been received then the names vector will be empty.
         *
         * @param joint_number The joint fo which to query the name
         * @return The name of the joint
         */
        boost::optional<std::string> get_joint_name(const size_t joint_number) const;
        
        /// Get the names of the of all the joints as reported by the joint state messages.
        std::vector<std::string> get_joint_names() const { return _joint_names; }
        
        /**
         * Get the number of joints present on this arm as reported by the joint state messages.
         * If no messages have been received then this value will be wrong.
         *
         * @return The number of joints, 0 if no messages have been received.
         */
        size_t get_joint_count() const { return _joint_names.size(); }
        
        /// Get the position of all of the joints of the arm, empty if no messages have arrived
        std::vector<double> get_current_joint_positions() const { return _current_joint_position; }
        
        /**
         * Get the position of a single joint of the arm. If the joint number is out of the valid
         * range a std::out_out_range error is thrown.
         *
         * Use DaVinciArm::get_joint_names to identify the name of the corresponding joint.
         *
         * @param joint_number The number of the joint to consider (starting from 0)
         * @return The position of the joint. This value is either in radians or mm.
         */
        boost::optional<double> get_current_joint_position(const size_t joint_number) const;
        
        /// Get the speed of all of the joints of the arm
        std::vector<double> get_current_joint_velocities() const { return _current_joint_velocity; }
        
        /**
         * Get the speed of a single joint of the arm. If the joint number is out of the valid
         * range a std::out_out_range error is thrown.
         *
         * Use DaVinciArm::get_joint_names to identify the name of the corresponding joint.
         *
         * @param joint_number The number of the joint to consider (starting from 0)
         * @return The speed of the joint. This value is either in radians/s or mm/s.
         */
        boost::optional<double> get_current_joint_velocity(const size_t joint_number) const;
        
        /// Get the effort of all of the joints of the arm
        std::vector<double> get_current_joint_efforts() const { return _current_joint_effort; }
        
        /**
         * Get the effort of a single joint of the arm. If the joint number is out of the valid
         * range a std::out_out_range error is thrown.
         *
         * Use DaVinciArm::get_joint_names to identify the name of the corresponding joint.
         *
         * @param joint_number The number of the joint to consider (starting from 0)
         * @return The speed of the joint.
         */
        boost::optional<double> get_current_joint_effort(const size_t joint_number) const;
        
        
        /**
         * Get the desired position of all the joint of the arm.
         *
         * @note:
         *  If the daVinci arm has not received any commands since being turned on then there will
         *  be no deired positions and the returned vector will be empty even if the current
         *  position vector is not empty.
         */
        std::vector<double> get_desired_joint_positions() const { return _desired_joint_position; }
        
        /**
         * Get the position of a single joint of the arm. If the joint number is out of the valid
         * range a std::out_out_range error is thrown.
         *
         * @note:
         *  If the daVinci arm has not received any commands since being turned on then there will
         *  be no deired positions and a quiet NaN will be returned instead.
         *
         * Use DaVinciArm::get_joint_names to identify the name of the corresponding joint.
         *
         * @param joint_number The number of the joint to consider (starting from 0).
         * @return The position of the joint. This value is either in radians or mm.
         */
        boost::optional<double> get_desired_joint_position(const size_t joint_number) const;
        
        /**
         * Get the desired velocity of all the joint of the arm.
         *
         * @note:
         *  If the daVinci arm has not received any commands since being turned on then there will
         *  be no deired velocities and the returned vector will be empty even if the current
         *  velocity vector is not empty.
         */
        std::vector<double> get_desired_joint_velocities() const { return _desired_joint_velocity; }
        
        /**
         * Get the speed of a single joint of the arm. If the joint number is out of the valid
         * range a std::out_out_range error is thrown.
         *
         * @note:
         *  If the daVinci arm has not received any commands since being turned on then there will
         *  be no deired positions and a quiet NaN will be returned instead.
         *
         * Use DaVinciArm::get_joint_names to identify the name of the corresponding joint.
         *
         * @param joint_number The number of the joint to consider (starting from 0)
         * @return The speed of the joint. This value is either in radians/s or mm/s.
         */
        boost::optional<double> get_desired_joint_velocity(const size_t joint_number) const;
        
        /**
         * Get the desired effort of all the joint of the arm.
         *
         * @note:
         *  If the daVinci arm has not received any commands since being turned on then there will
         *  be no deired efforts and the returned vector will be empty even if the current
         *  effort vector is not empty.
         */
        std::vector<double> get_desired_joint_efforts() const { return _desired_joint_effort; }
        
        /**
         * Get the effort of a single joint of the arm. If the joint number is out of the valid
         * range a std::out_out_range error is thrown.
         *
         * @note:
         *  If the daVinci arm has not received any commands since being turned on then there will
         *  be no deired positions and a quiet NaN will be returned instead.
         *
         * Use DaVinciArm::get_joint_names to identify the name of the corresponding joint.
         *
         * @param joint_number The number of the joint to consider (starting from 0)
         * @return The speed of the joint.
         */
        boost::optional<double> get_desired_joint_effort(const size_t joint_number) const;
        
        boost::optional<KDL::Frame> get_current_position() const;
        boost::optional<KDL::Frame> get_current_local_position() const;
        
        boost::optional<KDL::Frame> get_desired_position() const;
        boost::optional<KDL::Frame> get_desired_local_position() const;
        
        KDL::Twist get_current_twist() const { return _twist_body; }
        KDL::Wrench get_current_wrench() const { return _wrench_body; }
    
    public: /* ROS Callbacks */
        
        void on_current_state_updated(const std_msgs::String state);
        void on_desired_state_updated(const std_msgs::String state);
        void on_goal_reached_updated(const std_msgs::Bool reached);
        void on_state_joint_current_updated(const sensor_msgs::JointState joint_state);
        void on_state_joint_desired_updated(const sensor_msgs::JointState joint_state);
        void on_position_cartesian_desired_updated(const geometry_msgs::PoseStamped pose);
        void on_position_cartesian_current_updated(const geometry_msgs::PoseStamped pose);
        void on_position_cartesian_local_desired_updated(const geometry_msgs::PoseStamped pose);
        void on_position_cartesian_local_current_updated(const geometry_msgs::PoseStamped pose);
        void on_twist_body_current_updated(const geometry_msgs::TwistStamped twist);
        void on_wrench_body_current_updated(const geometry_msgs::WrenchStamped wrench);
        void on_jacobian_spatial_updated(const std_msgs::Float64MultiArray jacobian);
        void on_jacobian_body_updated(const std_msgs::Float64MultiArray jacobian);
    
    private:
        
        ros::NodeHandle _nh;
        
        /* State Variables */
        
        /// The name of this daVinci arm. Should be one of ECM, MTML, MTMR, PSM1, PSM2, PSM3 unless
        /// you have added new arms to the system.
        std::string _arm_name;
        
        /// The base ROS namespace for the arm.
        /// The arm topics will be assembled as dvrk_namespace / arm_name / topic.
        std::string _dvrk_namespace;
        
        /// The current state of the arm.
        std::string _current_state = "Unset";
        
        /// The `header.seq` field of the last received current state message
        uint32_t _current_state_seq = 0;
        
        /// The desired state of the arm.
        std::string _desired_state = "Unset";
        
        /// The `header.seq` field of the last received current state message
        uint32_t _desired_state_seq = 0;
        
        /// Has the goal for the last action been reached.
        bool _goal_reached = false;
        
        /// The `header.seq` field of the last received current joint state message
        uint32_t _current_joint_state_seq = 0;
        
        /// The names of all the joints as reported in the latests current joint state message
        std::vector<std::string> _joint_names;
        
        /// The current position of each joint
        std::vector<double> _current_joint_position;
        
        /// The current velocity of each joint
        std::vector<double> _current_joint_velocity;
        
        /// The current effort of each joint
        std::vector<double> _current_joint_effort;
        
        /// The `header.seq` field of the last received desired joint state message
        uint32_t _desired_joint_state_seq = 0;
        
        /// The desired position of each joint
        std::vector<double> _desired_joint_position;
        
        /// The desired velocity of each joint
        std::vector<double> _desired_joint_velocity;
        
        /// The desired effort of each joint
        std::vector<double> _desired_joint_effort;
        
        /// The `header.seq` field of the last received current pose message
        uint32_t _position_cartesian_current_seq = 0;
        
        /// The current position of the arm relative to its base frame
        KDL::Frame _position_cartesian_current;
        
        /// The `header.seq` field of the last received desired pose message
        uint32_t _position_cartesian_desired_seq = 0;
        
        /// The desired position of the arm relative to its base frame
        KDL::Frame _position_cartesian_desired;
        
        /// The `header.seq` field of the last received current local pose message
        uint32_t _position_cartesian_local_current_seq = 0;
        
        /// The current position of the arm relative to is centre-of-motion
        KDL::Frame _position_cartesian_local_current;
        
        /// The `header.seq` field of the last received desired local pose message
        uint32_t _position_cartesian_local_desired_seq = 0;
        
        /// The current position of the arm relative to is centre-of-motion
        KDL::Frame _position_cartesian_local_desired;
        
        KDL::Twist _twist_body;
        KDL::Wrench _wrench_body;
        
        /* ROS Stuff */
        
        ros::Subscriber _current_state_sub;
        ros::Subscriber _desired_state_sub;
        ros::Subscriber _goal_reached_sub;
        ros::Subscriber _state_joint_current_sub;
        ros::Subscriber _state_joint_desired_sub;
        ros::Subscriber _position_cartesian_desired_sub;
        ros::Subscriber _position_cartesian_current_sub;
        ros::Subscriber _position_cartesian_local_desired_sub;
        ros::Subscriber _position_cartesian_local_current_sub;
        ros::Subscriber _twist_body_current_sub;
        ros::Subscriber _wrench_body_current_sub;
        ros::Subscriber _jacobian_spatial_sub;
        ros::Subscriber _jacobian_body_sub;
        
        // TODO: Add publishers
        
    };
    
}
