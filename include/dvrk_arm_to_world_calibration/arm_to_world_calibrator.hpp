//
// Created by nima on 16/11/17.
//

#pragma once

#include <memory>

#include <boost/filesystem.hpp>
#include <opencv2/core.hpp>

#include <dvrk_common/ros/camera/simple_camera.hpp>
#include <dvrk_camera_calibration/calibration_image.hpp>

#include "dvrk_arm_to_world_calibration/davinci_arm.hpp"

namespace dvrk {
    
    class ArmToWorldCalibrator {
    public:
        enum class CalibrationProgress {
            WaitingForStart, InProgress, Complete
        };
        
    public:
        ArmToWorldCalibrator(
            std::shared_ptr<DaVinciArm>& arm,
            std::shared_ptr<SimpleCamera>& camera,
            const CalibrationPattern pattern
        );
        
        
        bool start();
        
        cv::Mat update();
        
        bool pick_point();
    
        boost::optional<KDL::Frame> calibrate();
        
        std::vector<cv::Point3f>& get_camera_points() { return _camera_points; }
        
        std::vector<cv::Point3f>& get_arm_points() { return _arm_points; }
        
        bool save_transformation(boost::filesystem::path filepath);
        
    private: /* Callbacks */
        
        void on_new_image(
            const sensor_msgs::ImageConstPtr& image,
            const image_geometry::PinholeCameraModel& model
        );
        
    private:
        
        /* State Variables */
        
        CalibrationProgress _progress = CalibrationProgress::WaitingForStart;
        
        CameraPose _initial_board_pose;
        
        boost::optional<CameraPose> _current_board_pose;
        
        boost::optional<CameraParams> _camera_params;
        
        dvrk::CalibrationImage _latest_image;
    
        /// When was the last arm point picked (poor man's debouncing);
        ros::Time _last_pick{ 0. };
    
    
        /* Configuration Variables */
        
        std::shared_ptr<SimpleCamera> _camera;
        
        std::shared_ptr<DaVinciArm> _arm;
        
        dvrk::CalibrationPattern _pattern;
        
        /// These are the coordinates of the points in world space. They are generated once at the
        /// start of the program.
        std::vector<cv::Point3f> _camera_points;
    
        /// These are the projections of the camera points into image space so that we can
        // draw them on the pattern.
        std::vector<cv::Point2f> _image_points;
        
        /// These are the coordinates of the points in arm space
        std::vector <cv::Point3f> _arm_points;
        
        boost::optional<KDL::Frame> _world_to_arm;
        
    };
    
}
