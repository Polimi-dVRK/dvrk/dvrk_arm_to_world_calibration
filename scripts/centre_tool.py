#!/usr/bin/env python

import dvrk
import sys, math, threading
import rospy, std_msgs
import numpy

def log(message):
    print rospy.get_caller_id(), '->', message


class PSMController:

    # configuration
    def configure(self, robot_name):
        print rospy.get_caller_id(), '-> configuring centre_tool for ', robot_name
        self.arm = dvrk.psm(robot_name)

    def start(self):
        log('-> Homing arm ...')

        if not self.arm.home():
            print rospy.get_caller_id(), 'Error: Unable to home robot'
            sys.exit(-1)

        # get current joints just to set size
        goal = numpy.copy(self.arm.get_current_joint_position())
        if len(goal) != 7:
            log('Unexpected number of joints. Should be 7 but was ' + len(goal))
            sys.exit(-1)

        # We want to reset the rotation of the wrist joints and outer rotation
        goal[3] = 0.0 # Outer Roll
        goal[4] = 0.0 # Wrist Pitch
        goal[5] = 0.0 # Writs Yaw
        goal[6] = 0.0 # End-Effector, Jaw Angle

        log('moving to zero position ...')
        if not self.arm.move_joint(goal):
            log('Error: Unable to move to starting position')
            sys.exit(-1)

        log('All done. Arm reset.')

if __name__ == '__main__':
    if len(sys.argv) < 2:
        print 'Error: Pass the name of the arm to reset as the first command line parameter'
        print '    e.g. rosrun dvrk_arm_to_world_calibration centre_tool PSM1'
        print ''

    try:
        rospy.init_node('centre_tool')

        application = PSMController()
        application.configure(sys.argv[0])
        application.start()

        log('-> Press ENTER to begin')
        raw_input()

    except rospy.ROSInterruptException:
        pass
